import Link from 'next/link'
import useSwr from 'swr'

const fetcher = (url) => fetch(url).then((res) => res.json())

export default function Index() {
  const { data, error } = useSwr('/api/hello', fetcher)

  if (error) return <div>Failed to load info</div>
  if (!data) return <div>Loading...</div>

  return (
    <body>
      <div class="ui container" id="wrapper">
        <h1>It's Page 2</h1>
        <div id="content" class="ui segment">
          <button class="ui primary button" id="personalize">
              <i class="settings icon"></i>
              <Link href={'/page1'}><a>Go to Page 1</a></Link>
          </button>
        </div>
        <p/>
        <h1>Team: {data.teamName}</h1>
        <h3>Points: {data.points}</h3>
      </div>
    </body>
  )
}
