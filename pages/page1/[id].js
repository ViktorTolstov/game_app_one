import Link from 'next/link'
import { useRouter } from 'next/router'

export default function Index() {
  const router = useRouter()
  const { id } = router.query

  return (
    <body>
      <div class="ui container" id="wrapper">
        <h1>It's Page 1 with id</h1>  
        <p>This ID is {id}</p>
        <div id="content" class="ui segment">
          <button class="ui primary button" id="personalize">
              <i class="settings icon"></i>
              <Link href={'/page1'}><a>Go to Main Page 1</a></Link>
          </button>
          <button class="ui primary button" id="personalize">
              <i class="settings icon"></i>
              <Link href={'/page2'}><a>Go to Page 2</a></Link>
          </button>
        </div>
      </div>
    </body>
  )
}
