import Link from 'next/link'

export default function Index() {
  return (
    <body>
      <div class="ui container" id="wrapper">
        <h1>It's Page 1</h1>
        <div id="content" class="ui segment">
          <button class="ui primary button" id="personalize">
              <i class="settings icon"></i>
              <Link href={'/page2'}><a>Go to Page 2</a></Link>
          </button>
        </div>
      </div>
    </body>
  )
}
